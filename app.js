var createError = require('http-errors');
var express = require('express');
var path = require('path');
var f = require("util").format;
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var db = require('./db')
var app = express();

var indexRouter = require('./routes/index');
var imageRouter = require("./routes/images");
var loginRouter = require("./routes/login");
var userRouter = require("./routes/users");
var tagsRouter = require("./routes/tags");
var adminRouter = require("./routes/admin");


var user = encodeURIComponent('varun');
var password = encodeURIComponent('var@woocation7#*');

var url = f('mongodb://%s:%s@localhost:27017/imagedb?authSource=admin',user,password);

var localurl = f('mongodb://localhost:27017/imagedb');
db.connect(url, function (err) {
	if (err) {
		console.log('Unable to connect to Mongo.')
		process.exit(1)
	} else {
		console.log('Listening on port 3001...')
	}
})


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});


app.use('/', indexRouter);
app.use("/image", imageRouter);
app.use("/login", loginRouter);
app.use("/users",userRouter);
app.use("/tags",tagsRouter);
app.use("/admin",adminRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
