var express = require('express');
var router = express.Router();
var db = require("../db");
var ObjectID = require('mongodb').ObjectID;

router.get("/all", function(req,res,next){
    var collection = db.get().db().collection("tags");
    collection.find({}).toArray(function(err,tags){
        if(err){
            res.status(500).send(err);
        } else {
            res.send(tags);
        }
    })
});

module.exports = router;