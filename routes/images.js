var express = require('express');
var router = express.Router();
var db = require("../db");
var ObjectID = require('mongodb').ObjectID;

//get all image collections
router.get("/all", function(req,res,next){
    db.get().db().listCollections().toArray(function(err,collections){
        if(err){
            res.status(500).send(err)
        } else {
            var cs = [];
            var counter = 0;
            collections.map(function(coll){
                var collection = db.get().db().collection(coll.name);
                collection.find({url:{$ne:null},tags:{$ne:null}}).toArray(function(colErr,images){
                    counter++;
                    if(images.length > 0){
                        cs.push(coll.name);
                    }

                    if(counter == collections.length -1){
                        res.send({total:cs});
                    }
                })
            })
        }
    })
})


router.get('/:userId', function (req, res, next) {
    //get a default image
    var userId = req.params.userId;
    var mapCollection = db.get().db().collection("user_collection_map");
    mapCollection.find({user_id:userId}).toArray(function(mapErr, userMaps){
        if(mapErr){
            res.status(500).send(mapErr);
        } else {
            try{
                var collectionNameToUse = userMaps[0].collection_name;
                var collection = db.get().db().collection(collectionNameToUse);
                collection.findOne({newTags:null,passToAdmin:null},function(err,images){
                    if(err){
                        res.status(500).send(err);
                    } else {
                        if(images == null){
                            res.send([]);
                        } else {
                            res.send(images);
                        }
                    }
                })
            } catch(e){
                res.send([]);
            }
        }
    })
});

router.get('/stats/:userId', function(req,res,next){
    var userId = req.params.userId;
    var mapCollection = db.get().db().collection("user_collection_map");
    mapCollection.find({user_id:userId}).toArray(function(mapErr, userMaps){
        if(mapErr){
            res.status(500).send(mapErr);
        } else {
            try{
                var collectionNameToUse = userMaps[0].collection_name;
                var collection = db.get().db().collection(collectionNameToUse);
                collection.find({"newTags": {$ne:null}}).toArray(function(notDoneErr, notProcessed){
                    var empty = 0;
                    notProcessed.map(function(np){
                        if(np.newTags.length == 0) empty++;
                    })
                    var objToSend = {
                        done:notProcessed.length,
                        empty:empty
                    };
                    collection.find({passToAdmin:{$ne:null}}).toArray(function(passToAdminErr, passed){
                        objToSend.passedToAdmin = passed.length;
                        res.send(objToSend);
                    });
                })
            } catch(e){
                res.send([]);
            }
        }
    })
})

router.post("/:userId",function (req, res, next) {
    var userId = req.params.userId;
    var mapCollection = db.get().db().collection("user_collection_map");
    mapCollection.find({user_id:userId}).toArray(function(mapErr, userMaps){
        if(mapErr){
            res.status(500).send(mapErr);
        } else {
            if(userMaps !== null && userMaps.length > 0){
                var collectionNameToUse = userMaps[0].collection_name;
                var collection = db.get().db().collection(collectionNameToUse);
                collection.updateOne({_id:ObjectID(req.body.id)},{$set:{newTags:req.body.newTags}},{ upsert: true }, function(err,response){
                    if(err){
                        res.status(500).send(err);
                    } else {
                        res.send(response);
                    }
                })
            } else {
                res.status(500).send("Error : User map not found");
            }
        }
    });
});

router.post("/passToAdmin/:userId",function (req, res, next) {
    var userId = req.params.userId;
    var mapCollection = db.get().db().collection("user_collection_map");
    mapCollection.find({user_id:userId}).toArray(function(mapErr, userMaps){
        if(mapErr){
            res.status(500).send(mapErr);
        } else {
            if(userMaps !== null && userMaps.length > 0){
                var collectionNameToUse = userMaps[0].collection_name;
                var collection = db.get().db().collection(collectionNameToUse);
                collection.updateOne({_id:ObjectID(req.body.id)},{$set:{passToAdmin:true}},{ upsert: true }, function(err,response){
                    if(err){
                        res.status(500).send(err);
                    } else {
                        res.send(response);
                    }
                })
            } else {
                res.status(500).send("Error : User map not found");
            }
        }
    });
})

module.exports = router;
