var express = require('express');
var router = express.Router();
var db = require("../db");
var bcrypt = require('bcrypt');

router.post("/", function(req,res, next){
    var uname = req.body.username;
    var pass = req.body.password;
    var collection = db.get().db().collection("user");
    collection.find({username:req.body.username}).toArray(function(err,user){
        if(err){
            res.status(500).send(err);
        } else {
            if(user.length == 0){
                res.status(403).send("Not found");
            } else {
                bcrypt.compare(pass,user[0].password).then(function(isSamePassword){
                    if(isSamePassword){
                        res.send(user);
                    } else {
                        res.status(403).send("Not Authenticated");
                    }
                });
            }
        }
    })
});

router.post("/admin", function(req,res, next){
    var uname = req.body.username;
    var pass = req.body.password;
    var collection = db.get().db().collection("admin");
    collection.find({username:req.body.username}).toArray(function(err,user){
        if(err){
            res.status(500).send(err);
        } else {
            if(user.length == 0){
                res.status(403).send("Not found");
            } else {
                bcrypt.compare(pass,user[0].password).then(function(isSamePassword){
                    if(isSamePassword){
                        res.send(user);
                    } else {
                        res.status(403).send("Not Authenticated");
                    }
                });
            }
        }
    })
});

module.exports = router;