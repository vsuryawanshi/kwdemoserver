var express = require('express');
var router = express.Router();
var db = require("../db");
var ObjectID = require('mongodb').ObjectID;


router.get("/stats", function(req,res,next){
    db.get().db().listCollections().toArray(function(err,collections){
        if(err){
            res.status(500).send(err)
        } else {
            var totalDone = 0;
            var counter = 0;
            collections.map(function(coll){
                var collection = db.get().db().collection(coll.name);
                collection.find({"newTags": {$ne:null}}).toArray(function(colErr,images){
                    counter++;
                    if(colErr){
                        return;
                    } else {
                        totalDone += images.length;
                    }
                    if(counter == collections.length -1){
                        res.send({total:totalDone});
                    }
                })
            })
        }
    })
});

router.get("/passed", function(req,res,next){
    db.get().db().listCollections().toArray(function(err,collections){
        if(err){
            res.status(500).send(err)
        } else {
            var imgArr = [];
            var counter = 0;
            collections.map(function(coll){
                var collection = db.get().db().collection(coll.name);
                collection.find({passToAdmin:true}).toArray(function(colErr,images){
                    counter++;
                    if(colErr){
                        return;
                    } else {
                        if(images.length > 0){
                            images.map(function(img){
                                img.collection_name = coll.name;
                                imgArr.push(img);
                            })
                        }
                    }
                    if(counter == collections.length -1){
                        res.send({total:imgArr});
                    }
                })
            })
        }
    })
});

router.post("/correctimage",function(req,res,next){
    var collection = db.get().db().collection(req.body.collection_name);
    collection.updateOne({_id:ObjectID(req.body.imgId)},{$set:{newTags:req.body.tags, passToAdmin:false}},{ upsert: true }, function(err,response){
        if(err){
            res.status(500).send(err);
        } else {
            res.send(response);
        }
    })
})

module.exports = router;